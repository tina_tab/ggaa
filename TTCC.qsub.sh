#!/bin/bash
## Tina ALAEITABAR - 2019

## run:
## qsub -v SAMPLE_PLAN='./input_ccs_fasta/fastq_R047_8samples/TTCC_SampleID_full.txt',ODIR='/data/users/talaeita/work/curie/TTCC/test_multi',GENOME='genomes/hg19/hg19.fa', SIGNATURE='CoreSignature-6SNP.csv',START_SEQ='GAGACCCTGTCT;AAAAAGAAAAGT',END_SEQ='CAGCATGATTCA;TTTAAACCATTC' -t 1-3 TTCC.qsub.sh
## qsub -v SAMPLE_PLAN='/data/kdi_prod/project_result/789/07.00/TTCC/data/ccs-fastq-R047/SampleID_Run047_TTCCscript.txt',ODIR='/data/kdi_prod/project_result/789/07.00/TTCC/R47',GENOME='genomes/hg19/hg19.fa',SIGNATURE='CoreSignature-6SNP.csv',START_SEQ='GAGACCCTGTCT;AAAAAGAAAAGT',END_SEQ='CAGCATGATTCA;TTTAAACCATTC' -t 1-48 TTCC.qsub.sh

## -t option ==> a parallel analysis in cluster for numbers of samples.
##Input data should be in fastq formated.
##InputFastq should be in the same path of SAMPLE_PLAN. Here my fastqs are in ./input_ccs_fasta/fastq_R047_8samples.

#####################################
###     Check options           #####
#####################################
if [[ -z ${SAMPLE_PLAN} || -z ${ODIR} || -z ${GENOME} || -z ${SIGNATURE} ]];then
    echo -e "Error - Missing parameter(s). Stop."
    exit -1
fi

if [ -z ${PBS_ARRAYID} ]; then
    PBS_ARRAYID=1
fi

arrayS=($(echo ${START_SEQ}|sed 's/;/\n/g'))
arrayE=($(echo ${END_SEQ}|sed 's/;/\n/g'))

if [[ ${#arrayS[@]} != ${#arrayE[@]} ]];then
    echo -e "Error - Missing start or end sequences. Stop."
    exit -1
fi

###################################
###     Set tools             #####
###################################
export PATH=/bioinfo/local/build/Centos/miniconda/miniconda3-4.5.12/bin:$PATH
source activate /bioinfo/local/build/Centos/envs_conda/basic_bioinfo_tools_20190521

#export PATH=/bioinfo/local/build/Centos/samtools/samtools-1.9/bin/:$PATH
#export PATH=/bioinfo/local/build/Centos/bedtools/bedtools-2.27.1/bin/:$PATH

#####################################
###     Creat variables         #####
#####################################
cd $PBS_O_WORKDIR
InputFastq=$(awk -v i=${PBS_ARRAYID} 'NR==i{print $1}' ${SAMPLE_PLAN})
InputPath=`dirname ${SAMPLE_PLAN}`
Sample_Name=`basename ${InputFastq}| sed -e 's/.fastq//'`
mkdir -p ${ODIR}/${Sample_Name}/tmp

#####################################
###     Convert fastq to fasta  #####
#####################################
inputfasta=`basename ${InputFastq}| sed -e 's/.fastq/.fasta/'`
sed -n '1~4s/^@/>/p;2~4p' ${InputPath}/${InputFastq} > ${ODIR}/${Sample_Name}/tmp/${inputfasta}

#########################################################################
###################  part 1) Find patterns ###############################
##########################################################################


#########################################
###     Extract Blue Zones  #############
#########################################

for index in ${!arrayE[*]}; do
  start_flanque_seq=${arrayS[$index]}
  end_flanque_seq=${arrayE[$index]}
  rev_start_flanque_seq=$(echo ${start_flanque_seq}|rev |tr "ATGC" "TACG")
  rev_end_flanque_seq=$(echo ${end_flanque_seq}|rev |tr "ATGC" "TACG")
  inputfasta=${ODIR}/${Sample_Name}/tmp/${Sample_Name}.fasta
  tmp_inputfasta=${ODIR}/${Sample_Name}/tmp/${start_flanque_seq}_${end_flanque_seq}_${Sample_Name}.fasta
  bedfile=${ODIR}/${Sample_Name}/tmp/blueZone_${start_flanque_seq}_${end_flanque_seq}_${Sample_Name}.bed
  bad_reads=${ODIR}/${Sample_Name}/tmp/bad_reads_${start_flanque_seq}_${end_flanque_seq}_${Sample_Name}.bed

  echo ""
  echo "*************** START To Find TTCC For:  ${Sample_Name} ***************"
  echo ""
  echo "Start_flanque_seq     ===> "   ${start_flanque_seq}
  echo "End_flanque_seq       ===> "    ${end_flanque_seq}
  echo ""
  echo "REV_start_flanque_seq ===> "    ${rev_start_flanque_seq}
  echo "REV_end_flanque_seq   ===> "    ${rev_end_flanque_seq}
  count=0
  count_rev=0
  for line in $(cat ${inputfasta}) ; do
        if grep -q '>' <<< $line ; then
                header=$(echo $line |sed -e "s/>//g")
        else
                if grep -q ${start_flanque_seq} <<< $line ; then
                        if grep -q ${end_flanque_seq} <<< $line ; then
                                pos_s1=$(echo $line|grep ${start_flanque_seq} -aob|wc -l)
                                pos_s2=$(echo $line|grep ${end_flanque_seq} -aob|wc -l )
                                if [[  ${pos_s1} -gt 1 || ${pos_s2} -gt 1  ]]; then
                                   printf '%s\t%s \n' ${header} "start_seq=yes, end_seq=yes but more than one bluezone">> ${bad_reads}
                                else
                                   s1=$(echo ${line}|grep ${start_flanque_seq} -aob|cut -d':' -f1)
                                   s2=$(echo ${line}|grep ${end_flanque_seq} -aob|cut -d':' -f1)
                                   s1=$(($s1+12))
                                   if [[ "$s1" -lt "$s2" ]]; then
                                        count=$(($count+1))
                                        printf '%s\t%s\t%s\t%s \n' ${header} ${s1} ${s2} ${line}>> ${bedfile}
                                        printf '%s%s\n%s\n' ">" ${header} ${line} >> ${tmp_inputfasta}
                                   else
                                     printf '%s\t%s \n' ${header} "start_seq=yes, end_seq=yes but start_seq > end_seq">> ${bad_reads}
                                   fi
                                 fi
                        else
                            if grep -q ${rev_start_flanque_seq} <<< $line ; then
                                   if grep -q ${rev_end_flanque_seq} <<< $line ; then
                                         rev_line=$(echo ${line}|rev |tr "ATGC" "TACG")
                                         pos_s1=$(echo ${line}|grep ${rev_start_flanque_seq} -aob | wc -l )
                                         pos_s2=$(echo ${line}|grep ${rev_end_flanque_seq} -aob |wc -l )
                                         if [[ ${pos_s1} -gt 1 || ${pos_s2} -gt 1 ]]; then
                                             printf '%s\t%s \n' ${header} "start_seq=Yes, rev-start-seq=Yes, but more than one bluezone in rev case!">> ${bad_reads}
                                         else
                                             s1=$(echo ${rev_line}|grep ${start_flanque_seq} -aob|cut -d':' -f1)
                                             s2=$(echo ${rev_line}|grep ${end_flanque_seq} -aob|cut -d':' -f1)
                                             s1=$(($s1+12))
                                             if [[ "$s1" -lt "$s2" ]]; then
                                                 count_rev=$(($count_rev+1))
                                                 printf '%s\t%s\t%s\t%s \n' ${header} ${s1} ${s2} ${rev_line}>> ${bedfile}
                                                 printf '%s%s\n%s\n' ">" ${header} ${rev_line} >> ${tmp_inputfasta}
                                             else
                                                 printf '%s\t%s \n' ${header} "rev_start=Yes,rev_end=No, seq rev_start_seq > rev_end_seq">> ${bad_reads}
                                             fi
                                          fi
                                   else
                                            printf '%s\t%s \n' ${header} "start_seq=Yes, end_seq=No, rev_start_seq=Yes, rev_end_seq = No">> ${bad_reads}
                                   fi
                            else
                                   printf '%s\t%s \n' ${header} "start_seq=Yes, end_seq=No, rev_start_seq=No">> ${bad_reads}
                            fi
                        fi
                else
                     if grep -q ${rev_start_flanque_seq} <<< $line ; then
                                   if grep -q ${rev_end_flanque_seq} <<< $line ; then
                                         rev_line=$(echo ${line}|rev |tr "ATGC" "TACG")
                                         pos_s1=$(echo $line|grep ${rev_start_flanque_seq} -aob|wc -l)
                                         pos_s2=$(echo $line|grep ${rev_end_flanque_seq} -aob|wc -l )
                                         if [[ ${pos_s1} -gt 1 || ${pos_s2} -gt 1 ]]; then
                                             printf '%s\t%s \n' ${header} "start_seq=No, rev-start-seq=Yes, but more than one bluezone in rev case!">> ${bad_reads}
                                         else
                                             s1=$(echo $rev_line|grep ${start_flanque_seq} -aob|cut -d':' -f1)
                                             s2=$(echo $rev_line|grep ${end_flanque_seq} -aob|cut -d':' -f1)
                                             s1=$(($s1+12))
                                             if [[ "$s1" -lt "$s2" ]]; then
                                                 count_rev=$(($count_rev+1))
                                                 printf '%s\t%s\t%s\t%s \n' ${header} ${s1} ${s2} ${rev_line}>> ${bedfile}
                                                 printf '%s%s\n%s\n' ">" ${header} ${rev_line} >> ${tmp_inputfasta}
                                             else
                                                 printf '%s\t%s \n' ${header} "There is rev-start seq rev_start_seq > rev_end_seq">> ${bad_reads}
                                             fi
                                         fi
                                   else
                                            printf '%s\t%s \n' ${header} "start_seq=No, rev_start_seq=yes, rev_end_seq = No">> ${bad_reads}
                                   fi
                     else
                            printf '%s\t%s \n' ${header} "start_seq=No,  rev_start_seq=No">> ${bad_reads}

                     fi
                fi
        fi
  done
  echo ""
  echo "*************** Extracted Blue Zones Is Done!"
  #############################################
  ###     Extract Seq of Blue Zones     #######
  #############################################
  if [ -s ${bedfile} ]; then 
    samtools faidx ${tmp_inputfasta}
    cmd="bedtools getfasta -fi ${tmp_inputfasta} -bed ${bedfile} -fo ${ODIR}/${Sample_Name}/tmp/${start_flanque_seq}_${end_flanque_seq}_${Sample_Name}.out -tab"
    eval $cmd
    echo ""
    echo   "*************** Extracted Seqeunce of Blue Zones Is Done!"

    while read line; do
        set $line
        header=$(echo $1)
        seq=$(echo $2|sed 's/TTCC/+/g'|sed '/^\s*$/d')
        trans=$(grep -Eo '[[:alpha:]]+|[+]+' <<<"$seq"|awk '{ print $0 "=" length($0) }'|sed 's/[A-Z]//g'|sed 's/^+./(TTCC)/g'|sed 's/=//g'|sed 's/+//g'|sed ':a;N;$!ba;s/\n/_/g')
        printf '%s\t%s\t%s\n' ${header} ${trans} ${seq}>> ${ODIR}/${Sample_Name}/tmp/${start_flanque_seq}_${end_flanque_seq}_${Sample_Name}.txt
    done < ${ODIR}/${Sample_Name}/tmp/${start_flanque_seq}_${end_flanque_seq}_${Sample_Name}.out
    echo ""
    echo   "*************** Translate Blue Zones Are Done!"
    echo ""
  fi

 ####################################################
 ###   Sort and count number of motifs  #############
 ####################################################
 if [ -s ${ODIR}/${Sample_Name}/tmp/${start_flanque_seq}_${end_flanque_seq}_${Sample_Name}.txt ]; then
     cat ${ODIR}/${Sample_Name}/tmp/${start_flanque_seq}_${end_flanque_seq}_${Sample_Name}.txt |awk '{gsub("^([0-9*_]+)","",$2)}1' OFS='\t'|cut -f 2|sed '/^[[:space:]]*$/d'|sort -V -r|uniq -c|sort -g -r > ${ODIR}/${Sample_Name}/CountPatterns_${start_flanque_seq}_${end_flanque_seq}_${Sample_Name}.txt
 fi

 ##############################
 ###    Make a Report       ###
 ##############################
 total_reads=$(grep -c "^>" ${inputfasta})
 perc_count=$(((count*100)/total_reads))
 perc_count_rev=$(((count_rev*100)/total_reads))
 sum_fr=$((count+count_rev))
 perc_sum_fr=$(((sum_fr*100)/total_reads))
 count_bad_reads=$(wc -l < ${bad_reads})
 perc_bad_reads=$(((count_bad_reads*100)/total_reads))

 echo "Number of total reads: "    $total_reads
 echo "Number of forwards reads: " $count      "==>" "%"$perc_count
 echo "Number of reverse reads: " $count_rev   "==>" "%"$perc_count_rev
 echo ""
 echo "Sum of forward & reverse: " $sum_fr     "==>" "%"$perc_sum_fr
 echo "Number of bad reads: "     $count_bad_reads "==>" "%"$perc_bad_reads

 ##printf '%s%s\n\n%s%s\t%s%s\n%s%s\t%s%s\n\n%s%s\t%s%s\n%s%s\t%s%s\n' "Number of total reads:  " ${total_reads} "Number of forwards reads:  " ${count} "%" ${perc_count} "Number of reverse reads:  " ${count_rev} "%" ${perc_count_rev} "Sum offorward & revers: " ${sum_fr} "%" ${perc_sum_fr} "Number of bad reads:  " ${count_bad_reads} "%" ${perc_bad_reads} >> ${ODIR}/${Sample_Name}/report_${start_flanque_seq}_${end_flanque_seq}_${Sample_Name}.txt

 echo ""

done

##########################################################################
###################  part 2) Find seeds    ###############################
##########################################################################

echo ""
echo "*************** Extracted seeds from ${SIGNATURE}! **************"

Seed_file=${SIGNATURE}
#####################################
###  Extract position of each seed ##
#####################################
grep -v 'RS' ${Seed_file}  > ${ODIR}/${Sample_Name}/tmp/tmp_Seed_file
while read myseed
do
       seed=$(echo ${myseed}|cut -f 4 -d " ")
       echo $seed
       snp_pos=$(echo ${myseed}|cut -f 5 -d " ")
       col1=$(echo ${myseed}|cut -f 1 -d " ")
       col2=$(echo ${myseed}|cut -f 2 -d " ")
       col3=$(echo ${myseed}|cut -f 3 -d " ")
       colname="${col1}_${col2}_${col3}"
       #echo $colname
       #echo ${snp_pos}
       len_seed=$(echo -n ${seed} | wc -c)
       #echo $len_seed
       Bad_file=${ODIR}/${Sample_Name}/tmp/${colname}_bad_files_${Sample_Name}.bed
       Bad_2times=${ODIR}/${Sample_Name}/tmp/${colname}_bad_2times_${Sample_Name}.bed
       Normal_file=${ODIR}/${Sample_Name}/tmp/${colname}_normal_reads_${Sample_Name}.bed
       Rev_file=${ODIR}/${Sample_Name}/tmp/${colname}_rev_reads_${Sample_Name}.bed
       Result_file=${ODIR}/${Sample_Name}/tmp/${colname}_result_${Sample_Name}.bed
       for line in $(cat ${inputfasta}) ; do
        if grep -q '>' <<< $line ; then
             header=$(echo $line |sed -e "s/>//g")
        else
             if grep -q ${seed} <<< $line ; then
                pos_s1=$(echo $line|grep ${seed} -aob|wc -l)
                if [[  ${pos_s1} -gt 1  ]]; then
                   printf '%s%s\n%s\n' ">" ${header} ${line} >> ${Bad_2times}
                else
                   pos_seed=$(echo $line|grep ${seed} -aob|cut -d':' -f1)
                   if [[ ${snp_pos} -gt 0 ]]; then
                         new_pos=$(echo "$(($pos_seed + $len_seed + $snp_pos))")
                   else
                         new_pos=$(echo "$(($pos_seed + $snp_pos + 1))")
                   fi
                   SNP=$(echo $line| cut -c ${new_pos})
                   printf '%s%s\t%s\n%s\n' ">" ${header} ${pos_seed} ${line}>> ${Normal_file}
                   printf '%s\t%s\t%s\n' ${header} "norm" ${SNP}>> ${Result_file}
                fi
             else
                rev_line=$(echo ${line}|rev |tr "ATGC" "TACG")
                if grep -q ${seed} <<< $rev_line ; then
                   pos_s1=$(echo $rev_line|grep ${seed} -aob|wc -l)
                   if [[  ${pos_s1} -gt 1  ]]; then
                      printf '%s%s\n%s\n' ">" ${header} ${rev_line} >> ${Bad_2times}
                   else
                     pos_seed=$(echo $rev_line|grep ${seed} -aob|cut -d':' -f1)
                     #echo ${header}
                     #echo ${pos_seed}
                     if [[ ${snp_pos} -gt 0 ]]; then
                         new_pos=$(echo "$(($pos_seed + $len_seed + $snp_pos ))")
                     else
                         new_pos=$(echo "$(($pos_seed + $snp_pos + 1 ))")
                     fi
                     SNP=$(echo $rev_line| cut -c ${new_pos})
                     printf '%s%s\t%s\n%s\n' ">" ${header} ${pos_seed} ${rev_line}>> ${Rev_file}
                     printf '%s\t%s\t%s\n' ${header} "rev" ${SNP}>> ${Result_file}
                   fi
                else
                   printf '%s%s\n%s\n' ">" ${header} ${line} >> ${Bad_file}

                fi
             fi
        fi
    done
done < ${ODIR}/${Sample_Name}/tmp/tmp_Seed_file

echo "********** Extracted SNP!"

##################################
###    merge all SNP         #####
##################################
##read files in order of position:
array=("$(ls ${ODIR}/${Sample_Name}/tmp/*_result_${Sample_Name}.bed|sort -t ":" -k 2 )")
for x in "${array[@]}";do echo "$x";done

echo ""
echo ""
num=$(echo "$array"|wc -l)

if [[ $num -gt 1 ]]; then
     echo "why not here"
     count=0
     for file in ${array[@]}
     do
        count=$(($count+1))
        echo "file" , $file
        if [ -f $file ]; then
               cat $file | sort -k 1,1 |cut -f 1,3 > $file.tmp
               echo ${file}.tmp
               if [[ ${count} -gt 2 ]]; then
                  str2_title=`basename ${file}.tmp| sed -e 's/_result_.*//'`
                  echo "${str2_title}"
                  str2_titles="${str2_titles} ${str2_title}"
                  str2="${str2} ${file}.tmp"
               else
                  str1_title=`basename ${file}.tmp| sed -e 's/_result_.*//'`
                  echo "${str1_title}"
                  str1_titles="${str1_titles} ${str1_title}"
                  str1="${str1} ${file}.tmp"
               fi
        fi
     done

     echo "\n"
     echo "str1: ",$str1
     echo "\n"
     echo "str2 ", $str2
     echo "\n"

     echo "title1: ",$str1_titles
     echo "\n"
     echo "title2: ",$str2_titles
     echo "\n"
     title="${str1_titles}${str2_titles}"
     echo $title


     firstOutput='0,1.2'; secondOutput='2.2'; myoutput="$firstOutput,$secondOutput"; outputCount=3; join -a 1 -a 2 -e 0 -o "$myoutput" ${str1} > tmp.tmp; for f in ${str2}; do firstOutput="$firstOutput,1.$outputCount"; myoutput="$firstOutput,$secondOutput"; join -a 1 -a 2 -e 0 -o "$myoutput" tmp.tmp $f > tempf; mv tempf tmp.tmp; outputCount=$(($outputCount+1)); done; mv tmp.tmp ${ODIR}/${Sample_Name}/files_join.txt

     echo "firstOutput='0,1.2'; secondOutput='2.2'; myoutput="$firstOutput,$secondOutput"; outputCount=3; join -a 1 -a 2 -e 0 -o "$myoutput" ${str1} > tmp.tmp; for f in ${str2}; do firstOutput="$firstOutput,1.$outputCount"; myoutput="$firstOutput,$secondOutput"; join -a 1 -a 2 -e 0 -o "$myoutput" tmp.tmp $f > tempf; mv tempf tmp.tmp; outputCount=$(($outputCount+1)); done; mv tmp.tmp ${ODIR}/${Sample_Name}/files_join.txt"
     
     for ((i=2;i<=$(($count+1));i++))
     do
         ibis="$"$i
         concat_snp="${concat_snp}$ibis"
         cols="${cols}"'"\t"'"$ibis"
     done

     cmd="awk '{ print "'$1'""'"\t"'"${concat_snp}${cols} }' ${ODIR}/${Sample_Name}/files_join.txt > ${ODIR}/${Sample_Name}/tmp/tmp.txt"
     eval "$cmd"

     rm ${ODIR}/${Sample_Name}/files_join.txt
     mv ${ODIR}/${Sample_Name}/tmp/tmp.txt ${ODIR}/${Sample_Name}/tmp/${Sample_Name}_files_join.txt

     echo "\n"
     cmd="sed -i '1s/^/readnames pattern${title} \n/' ${ODIR}/${Sample_Name}/tmp/${Sample_Name}_files_join.txt"
     eval "$cmd"
     cmd="sed -i 's/^[ \t]*//' ${ODIR}/${Sample_Name}/tmp/${Sample_Name}_files_join.txt"
     eval "$cmd"

else
      echo "comme here"
      title=`basename ${array[@]}| cut -d "_" -f 1,2,3`
      echo ${title}
      sort -k 1,1 ${array[@]}|awk -F '\t' '{print $1,$3,$3}'|uniq > ${ODIR}/${Sample_Name}/tmp/${Sample_Name}_files_join.txt
      cmd="sed -i '1s/^/readnames pattern ${title} \n/' ${ODIR}/${Sample_Name}/tmp/${Sample_Name}_files_join.txt"
      eval "$cmd"
      cmd="sed -i 's/^[ \t]*//' ${ODIR}/${Sample_Name}/tmp/${Sample_Name}_files_join.txt"
      eval "$cmd"
fi


#for ((i=2;i<=$(($count+1));i++))
#do
#   ibis="$"$i
#   concat_snp="${concat_snp}$ibis"
#   cols="${cols}"'"\t"'"$ibis"
#done

#cmd="awk '{ print "'$1'""'"\t"'"${concat_snp}${cols} }' ${ODIR}/${Sample_Name}/files_join.txt > ${ODIR}/${Sample_Name}/tmp/tmp.txt"
#eval "$cmd"

#rm ${ODIR}/${Sample_Name}/files_join.txt
#mv ${ODIR}/${Sample_Name}/tmp/tmp.txt ${ODIR}/${Sample_Name}/tmp/${Sample_Name}_files_join.txt

#echo "\n"
#cmd="sed -i '1s/^/readnames pattern${title} \n/' ${ODIR}/${Sample_Name}/tmp/${Sample_Name}_files_join.txt"
#eval "$cmd"
#cmd="sed -i 's/^[ \t]*//' ${ODIR}/${Sample_Name}/tmp/${Sample_Name}_files_join.txt"
#eval "$cmd"



##########################################################################
###################  part 3) Merge all    ###############################
##########################################################################

echo "********************* Merge all ! ********************************"
array=()
for index in ${!arrayE[*]}; do
  start_flanque_seq=${arrayS[$index]}
  end_flanque_seq=${arrayE[$index]}
  if [ -f ${ODIR}/${Sample_Name}/tmp/${start_flanque_seq}_${end_flanque_seq}_${Sample_Name}.txt ]; then
     array=("${array[@]}" ${ODIR}/${Sample_Name}/tmp/${start_flanque_seq}_${end_flanque_seq}_${Sample_Name}.txt)
  fi
done

#echo ${#array[@]}

str1=""
str2=""
str1_titles=""
str2_titles=""

if [[ ${#array[@]} -gt 1 ]]; then
   count=0
   for file in ${array[@]}
   do
     count=$(($count+1))
##     cat $file |sort -k 1,1 |cut -f 1,2 |awk '{gsub("(:[0-9*_-]+)","",$1)}1' OFS='\t'|awk '{gsub("^([0-9*_]+)","",$2)}1' OFS='\t'|awk '{gsub("^$","0",$2)}1' OFS='\t'|awk '{gsub("(_[0-9*_-]+)","_",$2)}1' OFS='\t'|awk '{gsub("_$","",$2)}1' OFS='\t'|uniq > $file.tmp
     cat $file |sort -k 1,1 |cut -f 1,2 |awk '{gsub("(:[0-9*_-]+)","",$1)}1' OFS='\t'|awk '{gsub("^([0-9*_]+)","",$2)}1' OFS='\t'|awk '{gsub("^$","0",$2)}1' OFS='\t'|awk '{gsub("(_[0-9]+$)","",$2)}1' OFS='\t'|uniq > $file.tmp
     if [[ ${count} -gt 2 ]]; then
         str2_title=`basename ${file}.tmp|cut -d "_" -f 1,2`
         echo "str2_title" ${str2_title}
         str2_titles="${str2_titles} ${str2_title}"
         str2="${str2} ${file}.tmp"
     else
         str1_title=`basename ${file}.tmp|cut -d "_" -f 1,2`
         echo "str1_title" ${str1_title}
         str1_titles="${str1_titles} ${str1_title}"
         str1="${str1} ${file}.tmp"
     fi
   done

   echo ""
   echo "str1: ",$str1
   echo ""
   echo "str2 ", $str2
   echo ""
   echo "title1: ",$str1_titles
   echo ""
   echo "title2: ",$str2_titles
   echo ""
   titlepatterns="${str1_titles}${str2_titles}"
   echo "titlepatterns: " ${titlepatterns}
   echo "title: " ${title}
   echo ""

   firstOutput='0,1.2'; secondOutput='2.2'; myoutput="$firstOutput,$secondOutput"; outputCount=3; join -a 1 -a 2 -e "NA" -o "$myoutput" ${str1} > tmp.tmp; for f in ${str2}; do firstOutput="$firstOutput,1.$outputCount"; myoutput="$firstOutput,$secondOutput"; join -a 1 -a 2 -e "NA" -o "$myoutput" tmp.tmp $f > tempf; mv tempf tmp.tmp; outputCount=$(($outputCount+1)); done; mv tmp.tmp ${ODIR}/${Sample_Name}/tmp/${Sample_Name}_patterns.txt

   echo ""
   echo "firstOutput='0,1.2'; secondOutput='2.2'; myoutput="$firstOutput,$secondOutput"; outputCount=3; join -a 1 -a 2 -e "NA" -o "$myoutput" ${str1} > tmp.tmp; for f in ${str2}; do firstOutput="$firstOutput,1.$outputCount"; myoutput="$firstOutput,$secondOutput"; join -a 1 -a 2 -e "NA" -o "$myoutput" tmp.tmp $f > tempf; mv tempf tmp.tmp; outputCount=$(($outputCount+1)); done; mv tmp.tmp ${ODIR}/${Sample_Name}/tmp/${Sample_Name}_patterns.txt"

   sed -i 's/ /\t/g' ${ODIR}/${Sample_Name}/tmp/${Sample_Name}_patterns.txt
   sort -k 1,1 ${ODIR}/${Sample_Name}/tmp/${Sample_Name}_patterns.txt|awk '{gsub("(:[0-9*_-]+)","",$1)}1' OFS='\t'|uniq > ${ODIR}/${Sample_Name}/tmp/${Sample_Name}_patterns.txt.sorted
else
   titlepatterns=`basename ${array[@]}| cut -d "_" -f 1,2`
   sort -k 1,1 ${array[@]}|awk '{gsub("^([0-9*_]+)","",$2)}1' OFS='\t'|awk '{gsub("^$","0",$2)}1' OFS='\t'|cut -f 1,2|awk '{gsub("(:[0-9*_-]+)","",$1)}1' OFS='\t'|uniq >  ${ODIR}/${Sample_Name}/tmp/${Sample_Name}_patterns.txt.sorted
fi


grep -v "readname" ${ODIR}/${Sample_Name}/tmp/${Sample_Name}_files_join.txt|sort -k 1,1  > ${ODIR}/${Sample_Name}/tmp/${Sample_Name}_files_join.txt.sorted
join -1 1 -2 1 ${ODIR}/${Sample_Name}/tmp/${Sample_Name}_patterns.txt.sorted ${ODIR}/${Sample_Name}/tmp/${Sample_Name}_files_join.txt.sorted > ${ODIR}/${Sample_Name}/Join_Pattern_TTCC_SNP_${Sample_Name}.txt
echo ""
echo "join -1 1 -2 1 ${ODIR}/${Sample_Name}/tmp/${Sample_Name}_patterns.txt.sorted ${ODIR}/${Sample_Name}/tmp/${Sample_Name}_files_join.txt.sorted > ${ODIR}/${Sample_Name}/Join_Pattern_TTCC_SNP_${Sample_Name}.txt"
cmd="sed -i '1s/^/readnames ${titlepatterns} pattern ${title} \n/' ${ODIR}/${Sample_Name}/Join_Pattern_TTCC_SNP_${Sample_Name}.txt"
echo $cmd
eval "$cmd"
cmd="sed -i 's/ /\t/g' ${ODIR}/${Sample_Name}/Join_Pattern_TTCC_SNP_${Sample_Name}.txt"
eval "$cmd"


firstLine=`head -1  ${ODIR}/${Sample_Name}/Join_Pattern_TTCC_SNP_${Sample_Name}.txt|sed 's/readnames/count/g'`
echo "firstLine" ${firstLine}
echo "grep -v ${ODIR}/${Sample_Name}/Join_Pattern_TTCC_SNP_${Sample_Name}.txt|awk '{$1=""; print $0}'|sort -V -r|sed 's/ /\t/g'|uniq -c|sort -g -r"
grep -v "readname" ${ODIR}/${Sample_Name}/Join_Pattern_TTCC_SNP_${Sample_Name}.txt|awk '{$1=""; print $0}'|sort -V -r|sed 's/ /\t/g'|uniq -c|sort -g -r > ${ODIR}/${Sample_Name}/Counts_Pattern_TTCC_SNP_${Sample_Name}.txt
cmd="sed -i '1s/^/${firstLine} \n/' ${ODIR}/${Sample_Name}/Counts_Pattern_TTCC_SNP_${Sample_Name}.txt"
echo $cmd
eval "$cmd"

##mv TTCC.qsub.sh.o* ${ODIR}/${Sample_Name}/tmp/TTCC.qsub.log
##mv TTCC.qsub.sh.e* ${ODIR}/${Sample_Name}/tmp/TTCC.qsub.error


##########################################################################
###################  part 4) Preparing data for IGV    ###################
##########################################################################

#echo "minimap2 -ax asm20 ${GENOME} ${inputfasta} > ${ODIR}/${Sample_Name}/tmp/hg19_${Sample_Name}.sam"
#minimap2 -ax asm20 ${GENOME} ${inputfasta} > ${ODIR}/${Sample_Name}/tmp/hg19_${Sample_Name}.sam
#samtools view ${ODIR}/${Sample_Name}/tmp/hg19_${Sample_Name}.sam -o ${ODIR}/${Sample_Name}/tmp/hg19_${Sample_Name}.bam
#samtools sort ${ODIR}/${Sample_Name}/tmp/hg19_${Sample_Name}.bam > ${ODIR}/${Sample_Name}/tmp/hg19_${Sample_Name}.sort.bam
#samtools index ${ODIR}/${Sample_Name}/tmp/hg19_${Sample_Name}.sort.bam

echo ""
echo "**********************************  Done :)   ***********************************"
